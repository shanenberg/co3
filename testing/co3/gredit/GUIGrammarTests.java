/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package co3.gredit;

import slope.Parsing;
import co3guigrammar.CO3GuiGrammarTokens;
import co3guigrammar.CO3GuiGrammarTokens.Grammar01Parser;
import junit.framework.TestCase;

public class GUIGrammarTests extends TestCase {
	
	public Object doLanguage(String s) {
		CO3GuiGrammarTokens tokens = new CO3GuiGrammarTokens();
		
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new Grammar01Parser().language());
		return null;
	}	
	
	public Object doGraphical(String s) {
		CO3GuiGrammarTokens tokens = new CO3GuiGrammarTokens();
		
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new Grammar01Parser().graphicalRule());
		return null;
	}	
	
	public Object doTestSpace(String s) {
		CO3GuiGrammarTokens tokens = new CO3GuiGrammarTokens();
		
		Parsing p = new Parsing(tokens, s); 
		p.parse(tokens.new Grammar01Parser().space());
		return null;
	}		
	public void testRealTests04() {
		/*
		   CancelButton: zB size [100, 100], size[=,=]
		   OKButton: zB size [50,50], size[?,?]
		   
		   CancelButton: zB size [100, 100], size[=,=]
		   OKButton: zB size [50,50], size[>,?]
		   
		   CancelButton: zB size [100, 100], size[=,=]
		   OKButton: zB size [50,50], size[?x,?x]
		 */
		
		
		
		doLanguage(
		  "OurTextBox"+
		  "  Requires          " + 
		  "  ButtonTexts.    "+
		  "  Main: vertical(Text,Buttons(equals(buttonSize, buttonSize))."+
		  "  Text: vertical(Headline, Paragraph)." +
		  "  Buttons(s1, s2): horizontal(OKButton(s1), CancelButton(s2))." + // hier wird die Höhe "ausgehandelt"
	  	  "  OKButton(s): <Button size: s>." +
	  	  "  CancelButton(s): <Button size: s fixed: s>.");
	}	

	public void testRealTests03() {
		doLanguage(
		  "OurTextBox"+
		  "  Requires          " + 
		  "  ButtonTexts.    "+
		  "  Main: vertical(Text,Buttons)."+
		  "  Text: vertical(Headline, Paragraph)." +
		  "  Buttons: horizontal(OKButton, CancelButton)." +
	  	  "  OKButton: <Button>.");
	}	
	
	public void testRealTests02() {
		doLanguage(
		  "OurTextBox"+
		  "  Requires          " + 
		  "  ButtonTexts.    "+
		  "  Main: vertical(Text,Buttons)."+
		  "  Text: vertical(Headline, Paragraph)." +
		  "  Buttons: horizontal(OKButton, CancelButton).");
	}	
	
	public void testRealTests01() {
		doLanguage(
		  "ourTextBox"+
		  "  Requires          " + 
		  "  ButtonTexts.    "+
		  "  Main: vertical(Text,Buttons).");

	}	

	
//	public void testAny() {
//		doLanguage("vertical( Title, Filebox, horizontal(OkButton, CancelButton))");
//	}	
	
	public void testHorizontalVertikalExpression() {
		doTestSpace("space (20pt)");
		//doTestWord("vertical[min=10, max=20, hresizable, vresizable, resizeChildren]()");
		doGraphical("vertical(space (20pt))");
		doGraphical("vertical(horizontal(space (20pt)))");
		doGraphical("vertical(horizontal(blbalbl))");
		doGraphical("vertical(horizontal(blbalbl, blablba))");
		doGraphical("vertical(horizontal(blbalbl, blablba),horizontal(blbalbl, blablba))");
		doGraphical("vertical(horizontal(blbalbl, vertical(space (20pt)), lkajdasd))");
		doGraphical("vertical(Title, Filebox, Buttons)");
		doGraphical("vertical( Title, Filebox, horizontal(OkButton, CancelButton))");
	}
	
	public void testSpaces() {
		doGraphical("vertical     ()");
	} 
	
	
	
	
}
