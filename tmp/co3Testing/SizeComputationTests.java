package co3Testing;
import co3.sizeComputation.AbsoluteOffset;
import co3.sizeComputation.AbsoluteSize;
import co3.sizeComputation.ChildRelativeSize;
import co3.sizeComputation.ParentRelativeSize;
import co3.uielement.UIElementParentChild;
import co3.util.Point2DFloat;
import junit.framework.TestCase;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

public class SizeComputationTests extends TestCase {

	public void testSizeComputationInsets() {
		UIElementParentChild p = new UIElementParentChild(new ChildRelativeSize());
		UIElementParentChild c1 = new UIElementParentChild(new AbsoluteSize(100, 100));
		p.addChild(c1);

		p.getSizeComputation().setLeftInset(new AbsoluteOffset(5,6));
		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(11,12));
		assertEquals(p.getSize(), new Point2DFloat(116, 118));
		
		p.getSizeComputation().setLeftInset(new AbsoluteOffset(-1000,-1000));
		assertEquals(p.getSize(), new Point2DFloat(0,0));
		p.getSizeComputation().setLeftInset(new AbsoluteOffset(-1000,-1));
		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(0,0));
		assertEquals(new Point2DFloat(0,99), p.getSize());
		
	}
		
	public void testSizeComputationOffsets() {
		UIElementParentChild p = new UIElementParentChild(new ChildRelativeSize());
		UIElementParentChild c1 = new UIElementParentChild(new AbsoluteSize(100, 100));
		p.addChild(c1);
		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(13,15));

		assertEquals(p.getSize(), new Point2DFloat(113, 115));

		c1.getSizeComputation().setRightOffset(new AbsoluteOffset(11,22));
		assertEquals(p.getSize(), new Point2DFloat(124, 137));

		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(-13,-15));
		assertEquals(p.getSize(), new Point2DFloat(98f, 107f));
		
		c1.getSizeComputation().setRightOffset(new AbsoluteOffset(0f,0f));
		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(-10,-20));
		assertEquals(p.getSize(), new Point2DFloat(90f, 80f));

		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(-110,-120));
		assertEquals(p.getSize(), new Point2DFloat(0f, 0f));
		
		c1.getSizeComputation().setLeftOffset(new AbsoluteOffset(-0,-0));
		c1.getSizeComputation().setRightOffset(new AbsoluteOffset(-110f,-120f));
		assertEquals(p.getSize(), new Point2DFloat(0f, 0f));		

		c1.getSizeComputation().setRightOffset(new AbsoluteOffset(-90f,-80f));
		assertEquals(p.getSize(), new Point2DFloat(10, 20));		
	}
	
	public void testChildRelativeSize() {
		UIElementParentChild p = new UIElementParentChild(new ChildRelativeSize());
		UIElementParentChild c1 = new UIElementParentChild(new AbsoluteSize(100f, 100f));
		p.addChild(c1);
		
		assertFalse(p.dependsOnParent());
		assertFalse(c1.dependsOnParent());
		assertEquals(p.getSize(), new Point2DFloat(100f, 100f));
		assertEquals(c1.getSize(), new Point2DFloat(100f, 100f));
		
		c1.setSc(new ChildRelativeSize());
		UIElementParentChild c2 = new UIElementParentChild(new AbsoluteSize(100f, 100f));
		c1.addChild(c2);
		
		assertFalse(p.dependsOnParent());
		assertFalse(c1.dependsOnParent());
		assertFalse(c2.dependsOnParent());
		assertEquals(p.getSize(), new Point2DFloat(100f, 100f));
		assertEquals(c1.getSize(), new Point2DFloat(100f, 100f));
		assertEquals(c2.getSize(), new Point2DFloat(100f, 100f));

		c2.setSc(new ParentRelativeSize());
		assertTrue(p.dependsOnParent());
		assertTrue(c1.dependsOnParent());
		assertTrue(c2.dependsOnParent());
		
		try {
			p.getSize();
			assertTrue("getSize should fail because of cyclic parent/child relationship", false);
		} catch (Exception ex) {}
		
	}
	
	
	public void testAbsolutSize() {
		UIElementParentChild p = new UIElementParentChild(new AbsoluteSize(100f, 100f));
		UIElementParentChild c = new UIElementParentChild(new AbsoluteSize(100f, 100f));
		p.addChild(c);
		
		assertFalse(p.dependsOnParent());
		assertFalse(c.dependsOnParent());
		
	}

}
