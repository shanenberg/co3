/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package co3.util;

import co3.sizeComputation.AbsoluteOffset;
import co3.sizeComputation.Offset;

public class Point2DFloat extends Point2D<Float>{

	public Point2DFloat(float x, float y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}
	
	public Point2DFloat clone() {
		return new Point2DFloat(x, y);
	}

	public Point2DFloat add(Point2DFloat point) {
		Point2DFloat ret = this.clone();
		ret.x = ret.x + point.x;
		ret.y = ret.y + point.y;
		return ret;
	}

	public Point2DFloat componentMaximum(Point2DFloat point2dFloat) {
		Point2DFloat ret = this.clone();
		if (point2dFloat.x > this.x)
			ret.x = point2dFloat.x;

		if (point2dFloat.y > this.y)
			ret.y = point2dFloat.y;

		return ret;
	}


}
