/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package co3.uielement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import co3.cmd.Cmd;
import co3.sizeComputation.SizeComputation;
import co3.util.Point2DFloat;
import junit.framework.TestCase;

/**

	Ein UIElement ist die abstrakte Oberklasse von grafischen Objekten. Ein UIElement kann eine 
	Reihe von Unterlementen haben, welche über Methoden a la setParent, .... verwaltet werden.
	
	Ein UIElement hat ein Feld getSizeObject, welches eine Strategy 


 */

public class UIElementParentChild {

	public UIElementParentChild(SizeComputation sc) {
		super();
		this.sc = sc;
	}

	UIElementParentChild parent;
	SizeComputation sc;

	public SizeComputation getSizeComputation() {
		return sc;
	}

	public void setSc(SizeComputation sc) {
		this.sc = sc;
	}

	final private ArrayList<UIElementParentChild> children = new ArrayList<UIElementParentChild>();

	/**
	 * AddChild fügt einem UIElement ein neues Kind hinzu.
	 *  - Wenn 
	 * @param child
	 */
	public void addChild(UIElementParentChild child) {
		child.setParent(this);		
		
		if (children.contains(child)) {
			children.remove(child);
		}
		
		children.add(child);
	}

	public void removeChild(UIElementParentChild child) {
		children.remove(child);
		if (child.getParent()!=null)
			child.setParent(null);
	}

	public void setParent(UIElementParentChild newParent) {
		if ((parent!=null) && (newParent != parent) && (newParent!=null)) {
			parent.removeChild(this);
			parent = newParent;
			newParent.addChild(this);
			return;
		}
		
		if ((parent!=null) && (newParent == null)) {
			UIElementParentChild tmp = parent;
			parent = null;
			tmp.removeChild(this);
			
			return;
		}		
		
		
		if ((parent==null) && (newParent == null)) {
			parent = null;
			return;
		}				

		if ((parent==null) && (newParent != null)) {
			parent = newParent;
			newParent.addChild(this);
			return;
		}		

		parent = newParent;
		

	}

	public UIElementParentChild getParent() {
		return parent;
	}

	public boolean hasChild(UIElementParentChild child) {
		return children.contains(child);
	}
	
	public List<UIElementParentChild> getChildren() {
		return children;
	}
	
	public void applyCommand(Cmd cmd) {
		cmd.doCmd(this);
	}

	public Point2DFloat getSize() {
		return this.sc.computeSize(this);
	}

	public boolean dependsOnParent() {
		return this.getSizeComputation().dependsOnParent(this);
	}	
	
}
