/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package co3.sizeComputation;

import co3.uielement.UIElementParentChild;
import co3.util.Point2DFloat;
import co3.util.XYFloat01Tuple;

public abstract class SizeComputation {
	
	Offset leftOffset, rightOffset;
	Offset leftInset, rightInset;
	
	
	public SizeComputation(Offset leftOffset, Offset rightOffset) {
		super();
		this.leftOffset = leftOffset;
		this.rightOffset = rightOffset;
	}
	
	public Offset getLeftOffset() {
		return leftOffset;
	}

	public void setLeftOffset(Offset leftOffset) {
		this.leftOffset = leftOffset;
	}

	public Offset getRightOffset() {
		return rightOffset;
	}

	public void setRightOffset(Offset rightOffset) {
		this.rightOffset = rightOffset;
	}

	public Offset getLeftInset() {
		return leftInset;
	}

	public void setLeftInset(Offset leftInSet) {
		this.leftInset = leftInSet;
	}

	public Offset getRightInset() {
		return rightInset;
	}

	public void setRightInset(Offset rightInSet) {
		this.rightInset = rightInSet;
	}

	public SizeComputation() {
		super();
		this.leftOffset = new AbsoluteOffset();
		this.leftOffset = new AbsoluteOffset();
		this.leftInset = new AbsoluteOffset();
		this.rightInset = new AbsoluteOffset();
	}	

	public abstract Point2DFloat computeSize(UIElementParentChild uiElement);
	
	public abstract boolean dependsOnParent(UIElementParentChild uiElement);
	
}
