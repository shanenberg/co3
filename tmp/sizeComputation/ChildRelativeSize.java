/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package co3.sizeComputation;

import co3.uielement.UIElementParentChild;
import co3.util.Point2DFloat;

/* Größe wird durch die Größe des Kindes beeinflusst 
 * 
 * Default Verhalten: Alle Kinder liegen übereinander, d.h. die maximum der xy-Werte der Kinder bestimmen Größe
 * 
 * 
 * */

public class ChildRelativeSize extends SizeComputation {

	@Override
	public Point2DFloat computeSize(UIElementParentChild uiElement) {
		boolean cannotAnswerQuestion = this.dependsOnParent(uiElement);
		if (cannotAnswerQuestion)
			throw new RuntimeException(
					"Invalid Configuration: ChildRelative sizes require at least one child that does not depend on parent");

		Point2DFloat ret = new Point2DFloat(0f, 0f);

		for (UIElementParentChild child : uiElement.getChildren()) {
			if (!child.dependsOnParent()) {
				Point2DFloat childSize = child.getSize();
				if (childSize.getX() > ret.getX()) {
					ret.setX(childSize.getX());
				}
				if (childSize.getY() > ret.getY()) {
					ret.setY(childSize.getY());
				}
			}
		}

		if (leftInset instanceof AbsoluteOffset) {
			ret = ret.add(((AbsoluteOffset) leftInset).getOffset());
		}

		if (rightInset instanceof AbsoluteOffset) {
			ret = ret.add(((AbsoluteOffset) rightInset).getOffset());
		}
		
		
		return ret.componentMaximum(new Point2DFloat(0, 0));
	}

	public boolean dependsOnParent(UIElementParentChild uiElement) {
		for (UIElementParentChild child : uiElement.getChildren()) {
			if (!child.dependsOnParent())
				return false;
		}
		return true;
	}

}
