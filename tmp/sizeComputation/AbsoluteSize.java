/**
* The MIT License (MIT)
*
* Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this 
* software and associated documentation files (the "Software"), to deal in the Software 
* without restriction, including without limitation the rights to use, copy, modify, 
* merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
* permit persons to whom the Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or 
* substantial portions of the Software.

* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package co3.sizeComputation;

import co3.uielement.UIElementParentChild;
import co3.util.Point2DFloat;

/* Wenn ein Element seine absolute Größe setzt, dann ist genau das die Größe -- egal, was jemand anders sagt.
 * Wenn man ein Parent nach seiner Größe fragt, dann ist die Größe die, die beschrieben ist.
 */

public class AbsoluteSize extends SizeComputation {

	Point2DFloat size;
	
	
	public AbsoluteSize(Point2DFloat size) {
		super();
		this.size = size;
	}
	
	public AbsoluteSize(float x, float y) {
		super();
		this.size = new Point2DFloat(x, y);
	}
	
	@Override
	public Point2DFloat computeSize(UIElementParentChild uiElement) {
		Point2DFloat ret = size.clone();

		if (leftOffset instanceof AbsoluteOffset) {
			AbsoluteOffset leftOffsetAbs = (AbsoluteOffset) leftOffset;
			ret = ret.add(leftOffsetAbs.offset);
		}
			
		if (rightOffset instanceof AbsoluteOffset) {
			AbsoluteOffset rightOffsetAbs = (AbsoluteOffset) rightOffset;
			ret = ret.add(rightOffsetAbs.offset);
		}
		
		return ret;
	}

	@Override
	public boolean dependsOnParent(UIElementParentChild uiElement) {
		return false;
	}
	
	

}
