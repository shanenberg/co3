Ziele für CO3:
==============

Wir möchten erst damit beginnen, einen gleinen grafischen Editor zu basteln. Wir wollen dazu:

1. Ein blaues Rechteck, was man auf eine Fläche ziehen kann. 
2. Ein rotes Rechteck, was man auf eine Fläche ziehen kann.
3. Ein grünes Rechteck, was man auf eine Fläche ziehen kann.

Das Verhalten soll wie folgt sein:
==================================
Wenn ich etwas auf mein blaues Rechteck ziehe, dann wird es rechts 
versetzt unterhalb des Rechtecks positioniert.

Beispiel: ich habe ein blaues Rechteck auf die Fläche gezogen, danach ein 
rotes Rechteck auf das blaue gezogen, daraufhin wird folgendes angezeigt:

     =======================
     |                     |
     |    blaues Rechteck  |
     |					   |
     =======================
             ========================
             |      rotes Rechteck  |
             ========================
             
Wenn ich etwas auf das rote Rechteck ziehe, dann wird es immer NEBEN das 
Rechteck gezeichnet. Wenn ich etwas auf das grüne ziehe, dann wird es IN
das Rechteck gezeichnet.

Ich kann mit mit dem einfachen Editor somit erst einmal eine kleine Zeichnung
von quasi-geschachtelten Rechtecken bauen.


Zum jetzigen Zeitpunkt wollen wir erst einmal ein "Gefühl" für die ganze Sache
bekommen, d.h. derzeit geben wir keine Techniken vor, wie es gelöst werden soll
und jeder kann einfach mal "herumspielen".

Ich (Stefan) werde damit beginnen, das ganze unter javax.swing zu machen, wo
man in einen JFrame reinmalen kann......wie gesagt....jedem ist erst einmal
freigestellt, worunter er es macht.

Das Ziel bei den nächsten Treffen ist es, sich gegenseitig den Code zu zeigen 
und darüber abzustimmen, welchen Ansatz wir verfolgen möchten.

Happy Coding

             
             