/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Stefan Hanenberg (stefan.hanenberg@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this 
 * software and associated documentation files (the "Software"), to deal in the Software 
 * without restriction, including without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

package co3guigrammar;

import slope.PC;
import slope.Parser;
import slope.ResultObject;
import slope.Rule;
import slope.SetCmd;
import slope.lib.grammar.parsetree.*;
import slowscane.Tokens;
import slowscane.annotations.Keyword;
import slowscane.annotations.Separator;
import slowscane.annotations.Token;
import slowscane.lib.Lambda01.Lambda01Tokens;
import slowscane.scanners.KeywordScanner;
import slowscane.scanners.SingleCharScanner;
import slowscane.scanners.TokenReader;

public class CO3GuiGrammarTokens extends Tokens {

	@Separator public TokenReader<?> WHITESPACE() {	return lib.WHITESPACE; 	}
	@Separator public TokenReader<?> NEWLINE() {	return lib.NEWLINE; }

	public final SingleCharScanner LargerThan = new SingleCharScanner('>');
	public final SingleCharScanner SmallerThan = new SingleCharScanner('<');
	
	@Keyword public TokenReader<?> COMMA() { return lib.COMMA;}
	@Keyword public TokenReader<?> TOKEN_ST() { return LargerThan;}
	@Keyword public TokenReader<?> TOKEN_LT() { return SmallerThan;}
	@Keyword public TokenReader<?> OBRACKET() { return lib.OPEN_BRACKET; }
	@Keyword public TokenReader<?> CBRACKET() { return lib.CLOSED_BRACKET; }
	
	@Keyword public TokenReader<?> KEYWORD_Requires() { return new KeywordScanner("Requires"); }	
	@Keyword public TokenReader<?> KEYWORD_DOT() { return lib.DOT; }	
	@Keyword public TokenReader<?> KEYWORD_COLON() { return lib.COLON; }	
	@Keyword public TokenReader<?> KEYWORD_PT() { return new KeywordScanner("pt"); }	
	@Keyword public TokenReader<?> KEYWORD_SPACE() { return new KeywordScanner("space"); }	
	@Keyword public TokenReader<?> KEYWORD_VERTICAL() { return new KeywordScanner("vertical"); }	
	@Keyword public TokenReader<?> KEYWORD_HORIZONTAL() { return new KeywordScanner("horizontal"); }	

	@Token public TokenReader<String> JAVA_IDENTIFIER() {	return lib.JAVA_IDENTIFIER; }
	@Token public TokenReader<?> JAVA_INTEGER() {	return lib.JAVA_INTEGER_LITERAL; }
	
	public class Grammar01Parser extends Parser<CO3GuiGrammarTokens> {

		/**
		 * Start -> FullQualifiedName
		 * @return
		 */
		public Rule language() { return new Rule() { public PC pc() {
			return
				AND( 
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN("Requires"),
					NFOLD(TOKEN(JAVA_IDENTIFIER())),
					TOKEN('.'),
					NFOLD(Element())
				);
		}};}			
		
		public Rule Element() { return new Rule() { public PC pc() {
			return
				AND( 
					TOKEN(JAVA_IDENTIFIER()),
					TOKEN(':'),
					expression(),
					TOKEN('.')
				);
		}};}			
		
		
		/**
		 *
		 * element -> ('horizontal' | 'vertical') '(' node (',' node)* ')'
		 * node -> element | space | javaIdentifier
		 * space -> 'space' JavaNumber 'pt'
		 * 
		 */
		public Rule expression() { return new Rule() { public PC pc() {
			return
				OR(
					guiWrapper(),
					graphicalRule()
				);
		}};}		

		public Rule guiWrapper() { return new Rule() { public PC pc() {
			return
				AND(
				  TOKEN('<'),
				  TOKEN(JAVA_IDENTIFIER()),
				  TOKEN('>')
				);
		}};}
		
		public Rule graphicalRule() { return new Rule() { public PC pc() {
			return
					AND(
						horizontalVertical(),
						TOKEN('('),
						OR(TOKEN(')'),
							AND(
							  OR(
							    AND(
								  node(),
								  NFOLD(
									AND(
									  TOKEN(','),
									  node()
									)
								  )
								),
								node()
							),
							TOKEN(')')
						)
					)
					);
		}};}	
		
		
		public Rule horizontalVertical() { return new Rule() { public PC pc() {
			return
				OR( 
					TOKEN("vertical"),
					TOKEN("horizontal")
				);
		}};}				
		
		public Rule node() { return new Rule() { public PC pc() {
			return
				OR(
						space(),
						expression(),
						TOKEN(JAVA_IDENTIFIER())
				);
		}};}		
	
		public Rule space() { return new Rule() { public PC pc() {
			return
				AND(
					TOKEN("space"),
					TOKEN('('),
					TOKEN(JAVA_INTEGER()),
					TOKEN("pt"),
					TOKEN(')')
				);
		}};}		
	}


}